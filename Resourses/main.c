#include "stm32f1xx.h"

#include "Timers.h"
#include "UART.h"
/* ----------------------- Modbus includes ----------------------------------*/
#include "mb.h"
#include "mbport.h"

/* ----------------------- Defines ------------------------------------------*/

/* ----------------------- Static variables ---------------------------------*/
static USHORT   usRegInputStart = REG_INPUT_START;
static USHORT   usRegInputBuf[REG_INPUT_NREGS];

static uint32_t lock_nesting_count = 0;
void __critical_enter(void)
{
	__disable_irq();
	++lock_nesting_count;
}
void __critical_exit(void)
{
	/* Unlock interrupts only when we are exiting the outermost nested call. */
	--lock_nesting_count;
	if (lock_nesting_count == 0) {
		__enable_irq();
	}
}



void delay(uint32_t time)
{
	uint32_t i = 0;
	for (i = 0; i < time; i++)
	{
		__NOP();
	}
}

int main(void)
{
	RCC->APB2ENR |= RCC_APB2ENR_IOPCEN;
	//PC13
	GPIOC->CRH &= ~(GPIO_CRH_CNF13_Msk | GPIO_CRH_MODE13_Msk);
	GPIOC->CRH |= (0b11 << GPIO_CRH_MODE13_Pos) | (0b00 << GPIO_CRH_CNF13_Pos);
	
	
	
	SYSCLK_Init();
	Timer2_Init();
	UART_Init(USART1);
	
	
	while (1)
	{
		
	}
}

