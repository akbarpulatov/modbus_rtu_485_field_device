#ifndef __TIMERS_H
#define __TIMERS_H


void SYSCLK_Init(void);
void Timer2_Init(void);

#define Nop() asm("NOP")

#endif // !__TIMERS_H
